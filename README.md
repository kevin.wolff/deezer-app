# Deezer app

Réalisé avec Express.js, React et l'API public de Deezer. Cette application permet de faire une copie des playlists d'un compte Deezer vers un autre.

Ainsi, lorsque vous souhaitez changer de compte car votre offre de x mois offerts est terminée :

- Créer un nouveau compte Deezer premium avec x mois offerts
- Transférer le contenu de votre ancien compte vers le nouveau

Voila, vous pouvez continuer à utiliser Deezer premium sans jamais avoir à payer, enjoy !

## Installation du projet

```shell
npm install
```

Configurer votre fichier .env

```shell
cp .env.dist .env
```

Lancer le serveur

```shell
node index.js
```

Hot reaload du serveur

```shell
nodemon index.js
```

Lancer l'application web

```shell
npm run start
```

Acceder à l'application web

http://localhost:3000/app




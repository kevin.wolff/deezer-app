require('dotenv').config()
const Express = require('express')
const cors = require('cors')
const Request = require('request')

const App = Express()

// Header for allow cors origin request.
App.use(cors());

// Server listener.
App.listen(process.env.SERVER_PORT, () => {
    console.log('Express is running')
})

// Redirect on Deezer.com authentication page.
App.get('/connexion', async (req, res) => {
    res.redirect(`${process.env.BASE_AUTH_URL}/auth.php?app_id=${process.env.APP_ID}&redirect_uri=${process.env.REDIRECT_URL}&perms=basic_access,offline_access,manage_library,delete_library`)
})

// When authenticated on Deezer.com get access token from authentication call back.
App.get('/auth', async (req, res) => {
    if (req.query.error_reason) {
        // Handle error // WIP
        res.redirect(process.env.WEB_APP_URL)
    }
    if (req.query.code) {
        await Request(`${process.env.BASE_AUTH_URL}/access_token.php?app_id=${process.env.APP_ID}&secret=${process.env.SECRET_KEY}&code=${req.query.code}&output=json`, (e, r, body) => {
            if (e) {
                // Handle error // WIP
                res.redirect(process.env.WEB_APP_URL)
            } else {
                const data = JSON.parse(body)
                res.redirect(`${process.env.WEB_APP_URL}?access_token=${data.access_token}`)
            }
        })
    }
})

// Get playlists.
App.get('/get/playlists/:accountId', async (req, res) => {
    Request(`${process.env.BASE_API_URL}/user/${req.params.accountId}/playlists`, (e, r, body) => {
        if (e) {
            // Handle error // WIP
            res.send('error')
        } else {
            const data = JSON.parse(body)
            res.send(data)
        }
    })
})

// Add playlist.
App.get('/add/playlist/:accountId/:playlistTitle/:token', async (req, res) => {
    await Request(`${process.env.BASE_API_URL}/user/${req.params.accountId}/playlists?request_method=POST&title=${req.params.playlistTitle}&access_token=${req.params.token}`, (e, r, body) => {
        if (e) {
            // Handle error // WIP
            res.send('error')
        } else {
            const data = JSON.parse(body)
            res.send(data)
        }
    })
})

// Delete playlist.
App.get('/delete/playlist/:playlistId/:token', async (req, res) => {
    await Request(`${process.env.BASE_API_URL}/playlist/${req.params.playlistId}?request_method=DELETE&access_token=${req.params.token}`, (e, r, body) => {
        if (e) {
            // Handle error // WIP
            res.send('error')
        } else {
            const data = JSON.parse(body)
            res.send(data)
        }
    })
})


// Get tracks.
App.get('/get/tracks/:accountId', async (req, res) => {
    Request(`${process.env.BASE_API_URL}/playlist/${req.params.accountId}`, (e, r, body) => {
        if (e) {
            // Handle error // WIP
            res.send('error')
        } else {
            const data = JSON.parse(body)
            res.send(data)
        }
    })
})

// Add track.
App.get('/add/track/:playlistId/:trackId/:token', async (req, res) => {
    await Request(`${process.env.BASE_API_URL}/playlist/${req.params.playlistId}/tracks?request_method=POST&songs=${req.params.trackId}&access_token=${req.params.token}`, (e, r, body) => {
        if (e) {
            // Handle error // WIP
            res.send('error')
        } else {
            const data = JSON.parse(body)
            res.send(data)
        }
    })
})

// Add favorite.
App.get('/add/favorite/:accountId/:trackId/:token', async (req, res) => {
    await Request(`${process.env.BASE_API_URL}/user/${req.params.accountId}/tracks?request_method=POST&track_id=${req.params.trackId}&access_token=${req.params.token}`, (e, r, body) => {
        if (e) {
            // Handle error // WIP
            res.send('error')
        } else {
            const data = JSON.parse(body)
            res.send(data)
        }
    })
})

// Delete favorite.
App.get('/delete/favorite/:accountId/:trackId/:token', async (req, res) => {
    await Request(`${process.env.BASE_API_URL}/user/${req.params.accountId}/tracks?request_method=DELETE&track_id=${req.params.trackId}&access_token=${req.params.token}`, (e, r, body) => {
        if (e) {
            // Handle error // WIP
            res.send('error')
        } else {
            const data = JSON.parse(body)
            res.send(data)
        }
    })
})

// Get artists.
App.get('/get/artist/:accountId', async (req, res) => {
    await Request(`${process.env.BASE_API_URL}/user/${req.params.accountId}/artists`, (e, r, body) => {
        if (e) {
            // Handle error // WIP
            res.send('error')
        } else {
            const data = JSON.parse(body)
            res.send(data)
        }
    })
})

// Add artist.
App.get('/add/artist/:accountId/:artistId/:token', async (req, res) => {
    await Request(`${process.env.BASE_API_URL}/user/${req.params.accountId}/artists?request_method=POST&artist_id=${req.params.artistId}&access_token=${req.params.token}`, (e, r, body) => {
        if (e) {
            // Handle error // WIP
            res.send('error')
        } else {
            const data = JSON.parse(body)
            res.send(data)
        }
    })
})

// Delete artist.
App.get('/delete/artist/:accountId/:artistId/:token', async (req, res) => {
    await Request(`${process.env.BASE_API_URL}/user/${req.params.accountId}/artists?request_method=DELETE&artist_id=${req.params.artistId}&access_token=${req.params.token}`, (e, r, body) => {
        if (e) {
            // Handle error // WIP
            res.send('error')
        } else {
            const data = JSON.parse(body)
            res.send(data)
        }
    })
})

// Get albums.
App.get('/get/album/:accountId', async (req, res) => {
    Request(`${process.env.BASE_API_URL}/user/${req.params.accountId}/albums`, (e, r, body) => {
        if (e) {
            // Handle error // WIP
            res.send('error')
        } else {
            const data = JSON.parse(body)
            res.send(data)
        }
    })
})

// Add album.
App.get('/add/album/:accountId/:albumId/:token', async (req, res) => {
    await Request(`${process.env.BASE_API_URL}/user/${req.params.accountId}/albums?request_method=POST&album_id=${req.params.albumId}&access_token=${req.params.token}`, (e, r, body) => {
        if (e) {
            // Handle error // WIP
            res.send('error')
        } else {
            const data = JSON.parse(body)
            res.send(data)
        }
    })
})

// Delete album.
App.get('/delete/album/:accountId/:albumId/:token', async (req, res) => {
    await Request(`${process.env.BASE_API_URL}/user/${req.params.accountId}/albums?request_method=DELETE&album_id=${req.params.albumId}&access_token=${req.params.token}`, (e, r, body) => {
        if (e) {
            // Handle error // WIP
            res.send('error')
        } else {
            const data = JSON.parse(body)
            res.send(data)
        }
    })
})
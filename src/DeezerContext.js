import {createContext} from 'react'

export default createContext({
    accessToken: String,
    setAccessToken: Function,
    oldAccountId: String,
    setOldAccountId: Function,
    currentAccountId: String,
    setCurrentAccountId: Function,
    content: Array,
    setContent: Function,
})
import {useState} from 'react'
import AppContext from './DeezerContext.js'
import StepAuth from './component/StepAuth.js'
import StepExport from './component/StepExport.js'
import StepImport from './component/StepImport.js'
import './App.scss'

export default function App() {
    const [accessToken, setAccessToken] = useState(String)
    const [oldAccountId, setOldAccountId] = useState(String)
    const [currentAccountId, setCurrentAccountId] = useState(String)
    const [content, setContent] = useState(Object)

    const AppContextValue = {
        accessToken,
        setAccessToken,
        oldAccountId,
        setOldAccountId,
        currentAccountId,
        setCurrentAccountId,
        content,
        setContent
    }

    return (
        <AppContext.Provider value={AppContextValue}>
            <div className="app">
                <StepAuth/>
                <StepExport/>
                <StepImport/>
            </div>
        </AppContext.Provider>
    )
}
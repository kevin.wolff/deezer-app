import State from '../class/State.js'
import Playlist from '../class/Playlist.js'
import Track from '../class/Track.js'
import Favorite from '../class/Favorite.js'
import Artist from '../class/Artist.js'
import Album from '../class/Album.js'

// Import content to a user.
export default async function importContent(accessToken, currentAccountId, content) {

    for (const playlistData of content.playlists) {

        // If playlist isn't a "favorite track", create a new playlist.
        if (!playlistData.isLovedTrack) {

            const playlistIndex = content.playlists.indexOf(playlistData)
            const addPlaylistResult = await Playlist.add(playlistIndex, currentAccountId, accessToken)
            if (addPlaylistResult.status !== 'success') return addPlaylistResult

            for (const track of playlistData.data) {
                const addTrackResult = await Track.add(addPlaylistResult.data.id, track, accessToken)
                if (addTrackResult.status !== 'success') return addTrackResult
            }

        } else {

            // else, compare current and new "favorite track" playlist to avoid double.
            const getPlaylistsResult = await Playlist.get(currentAccountId)
            if (getPlaylistsResult.status !== 'success') return getPlaylistsResult

            for (const currentPlaylist of getPlaylistsResult.data) {
                if (currentPlaylist.is_loved_track) {

                    const currentFavoriteTracks = await Track.get(currentPlaylist.id)
                    if (currentFavoriteTracks.status !== 'success') return currentFavoriteTracks

                    for (const track of playlistData.data) {
                        if (!currentFavoriteTracks.data.includes(track)) {
                            const addFavoriteResult = await Favorite.add(currentAccountId, track, accessToken)
                            if (addFavoriteResult.status !== 'success') return addFavoriteResult
                        }
                    }
                }
            }
        }
    }

    // Add favorite artists.
    for (const artistId of content.artists) {
        const addArtistResult = await Artist.add(currentAccountId, artistId, accessToken)
        if (addArtistResult.status !== 'success') return addArtistResult
    }

    // Add favorite albums.
    for (const albumId of content.albums) {
        const addAlbumResult = await Album.add(currentAccountId, albumId, accessToken)
        if (addAlbumResult.status !== 'success') return addAlbumResult
    }

    return new State(
        'success_import',
        'content imported with success',
        'import content',
    )
}
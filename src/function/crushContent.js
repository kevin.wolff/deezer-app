import State from '../class/State.js'
import Playlist from '../class/Playlist.js'
import Track from '../class/Track.js'
import Favorite from '../class/Favorite.js'
import Artist from '../class/Artist.js'
import Album from '../class/Album.js'

// Delete a user's content.
export default async function crushContent(currentAccountId, accessToken, contentType) {

    // Delete a user's playlists.
    if (contentType === 'playlist') {
        const getPlaylistsResult = await Playlist.get(currentAccountId)
        if (getPlaylistsResult.status !== 'success') return getPlaylistsResult

        for (const playlist of getPlaylistsResult.data) {
            if (!playlist.is_loved_track) {
                const deletePlaylistResult = await Playlist.delete(playlist.id, accessToken)
                if (deletePlaylistResult.status !== 'success') return deletePlaylistResult
            }
        }
    }

    // Delete a user's favorite tracks.
    if (contentType === 'favorite') {
        const getPlaylistsResult = await Playlist.get(currentAccountId)
        if (getPlaylistsResult.status !== 'success') return getPlaylistsResult

        for (const playlist of getPlaylistsResult.data) {
            if (playlist.is_loved_track) {
                const getFavoriteTracksResult = await Track.get(playlist.id)
                if (getFavoriteTracksResult.status !== 'success') return getFavoriteTracksResult

                for (const favoriteTrack of getFavoriteTracksResult.data) {
                    const deleteFavoriteResult = await Favorite.delete(currentAccountId, favoriteTrack, accessToken)
                    if (deleteFavoriteResult.status !== 'success') return deleteFavoriteResult
                }
            }
        }
    }

    // Delete a user's favorite artists.
    if (contentType === 'artist') {
        const getArtistsResult = await Artist.get(currentAccountId)
        if (getArtistsResult.status !== 'success') return getArtistsResult

        for (const artist of getArtistsResult.data) {
            const deleteArtistResult = await Artist.delete(currentAccountId, artist.id, accessToken)
            if (deleteArtistResult.status !== 'success') return deleteArtistResult
        }
    }

    // Delete a user's favorite albums.
    if (contentType === 'album') {
        const getAlbumsResult = await Album.get(currentAccountId)
        if (getAlbumsResult.status !== 'success') return getAlbumsResult

        for (const album of getAlbumsResult.data) {
            const deleteAlbumResult = await Album.delete(currentAccountId, album.id, accessToken)
            if (deleteAlbumResult.status !== 'success') return deleteAlbumResult
            console.log(deleteAlbumResult)
        }
    }

    return new State(
        `success_crush_${contentType}`,
        'content crushed with success',
        'crush content'
    )
}
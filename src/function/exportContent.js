import State from '../class/State.js'
import Playlist from '../class/Playlist.js'
import Track from '../class/Track.js'
import Artist from '../class/Artist.js'
import Album from '../class/Album.js'

// Get content from a user.
export default async function exportContent(oldAccountId) {
    const playlists = []
    const artists = []
    const albums = []
    let favoriteTracksLength = 0
    let playlistsLength = 0
    let artistsLength = 0
    let albumsLength = 0

    const getPlaylistsResult = await Playlist.get(oldAccountId)
    if (getPlaylistsResult.status !== 'success') return getPlaylistsResult

    const getArtistsResult = await Artist.get(oldAccountId)
    if (getArtistsResult.status !== 'success') return getArtistsResult

    const getAlbumsResult = await Album.get(oldAccountId)
    if (getAlbumsResult.status !== 'success') return getAlbumsResult

    for (const playlistData of getPlaylistsResult.data) {

        const getTracksResult = await Track.get(playlistData.id)
        if (getTracksResult.status !== 'success') return getTracksResult

        if (!playlistData.is_loved_track) {
            playlistsLength += 1
            playlists.push(new Playlist(playlistData.id, getTracksResult.data, false))
        } else {
            getTracksResult.data.forEach(() => {
                favoriteTracksLength += 1
            })
            playlists.push(new Playlist(playlistData.id, getTracksResult.data, true))
        }
    }

    for (const artist of getArtistsResult.data) {
        artistsLength += 1
        artists.push(artist.id)
    }

    for (const album of getAlbumsResult.data) {
        albumsLength += 1
        albums.push(album.id)
    }

    return new State(
        'success_export',
        'content exported with success',
        'export content',
        {
            playlists,
            artists,
            albums,
            favoriteTracksLength,
            playlistsLength,
            artistsLength,
            albumsLength
        }
    )
}
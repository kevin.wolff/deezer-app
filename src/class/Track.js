import Axios from 'axios'
import state from './State.js'

export default class Track {

    // Get playlist's tracks.
    static async get(playlistId) {
        const trackList = []

        const result = await Axios.get(`http://127.0.0.1:8080/get/tracks/${playlistId}`)
        if (result.data.error || result.status !== 200) {
            return new state(
                'error',
                result.data.error.message ? result.data.error.message : 'error when getting playlist\' tracks',
                'get tracks'
            )
        }

        for (const track of result.data.tracks.data) {
            trackList.push(track.id)
        }

        return new state(
            'success',
            'got playlist\' tracks with success',
            'get tracks',
            trackList
        )
    }

    // Add track to playlist.
    static async add(playlistId, trackId, token) {

        const result = await Axios.get(`http://127.0.0.1:8080/add/track/${playlistId}/${trackId}/${token}`)
        if (result.data.error || result.status !== 200) {
            return new state(
                'error',
                result.data.error.message ? result.data.error.message : 'error when creating a playlist\' track',
                'create playlist track'
            )
        }

        return new state(
            'success',
            'playlist\' track created with success',
            'create playlist track'
        )
    }
}
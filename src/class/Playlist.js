import Axios from 'axios'
import state from './State.js'

export default class playlist {
    constructor(id, data, isLovedTrack) {
        this.id = id
        this.data = data
        this.isLovedTrack = isLovedTrack
    }

    // Get playlists.
    static async get(accountId) {

        const result = await Axios.get(`http://127.0.0.1:8080/get/playlists/${accountId}`)
        if (result.data.error || result.status !== 200) {
            return new state(
                'error',
                result.data.error ? result.data.error.message : 'error when getting playlists',
                'get playlists'
            )
        }

        return new state(
            'success',
            'got playlists with success',
            'get playlists',
            result.data.data
        )
    }

    // Add playlist.
    static async add(playlistIndex, accountId, token) {
        const playlistTitle = `playlist-${playlistIndex}`

        const result = await Axios.get(`http://127.0.0.1:8080/add/playlist/${accountId}/${playlistTitle}/${token}`)
        if (result.data.error || result.status !== 200) {
            return new state(
                'error',
                result.data.error.message ? result.data.error.message : 'error when creating playlist',
                'create playlist'
            )
        }

        return new state(
            'success',
            'playlist created with success',
            'create playlist',
            result.data
        )
    }

    // Remove playlist.
    static async delete(playlistId, token) {

        const result = await Axios.get(`http://127.0.0.1:8080/delete/playlist/${playlistId}/${token}`)
        if (result.data.error || result.status !== 200) {
            return new state(
                'error',
                result.data.error.message ? result.data.error.message : 'error when deleting playlist',
                'remove playlist'
            )
        }

        return new state(
            'success',
            'playlist removed with success',
            'remove playlist'
        )
    }
}
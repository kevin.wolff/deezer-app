import Axios from 'axios'
import state from './State.js'

export default class Artist {

    // Get artists.
    static async get(accountId) {

        const result = await Axios.get(`http://127.0.0.1:8080/get/artist/${accountId}`)
        if (result.data.error || result.status !== 200) {
            return new state(
                'error',
                result.data.error ? result.data.error.message : 'error when getting artists',
                'get artists'
            )
        }

        return new state(
            'success',
            'got artists with success',
            'get artists',
            result.data.data
        )
    }

    // Add artist.
    static async add(accountId, artistId, token) {

        const result = await Axios.get(`http://127.0.0.1:8080/add/artist/${accountId}/${artistId}/${token}`)
        if (result.data.error || result.status !== 200) {
            return new state(
                'error',
                result.data.error.message ? result.data.error.message : 'error when adding artist',
                'add artist'
            )
        }

        return new state(
            'success',
            'artist added with success',
            'add artist'
        )
    }

    // Remove artist.
    static async delete(accountId, artistId, token) {

        const result = await Axios.get(`http://127.0.0.1:8080/delete/artist/${accountId}/${artistId}/${token}`)
        if (result.data.error || result.status !== 200) {
            return new state(
                'error',
                result.data.error.message ? result.data.error.message : 'error when removing artist',
                'remove artist'
            )
        }

        return new state(
            'success',
            'artist removed with success',
            'remove artist'
        )
    }
}
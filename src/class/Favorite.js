import Axios from 'axios'
import state from './State.js'

export default class Favorite {

    // Add favorite.
    static async add(accountId, trackId, token) {

        const result = await Axios.get(`http://127.0.0.1:8080/add/favorite/${accountId}/${trackId}/${token}`)
        if (result.data.error || result.status !== 200) {
            return new state(
                'error',
                result.data.error.message ? result.data.error.message : 'error when adding a favorite',
                'add favorite track'
            )
        }

        return new state(
            'success',
            'favorite added with success',
            'add favorite track'
        )
    }

    // Remove favorite.
    static async delete(accountId, favoriteId, token) {

        const result = await Axios.get(`http://127.0.0.1:8080/delete/favorite/${accountId}/${favoriteId}/${token}`)
        if (result.data.error || result.status !== 200) {
            return new state(
                'error',
                result.data.error.message ? result.data.error.message : 'error when deleting a favorite',
                'remove favorite'
            )
        }

        return new state(
            'success',
            'favorite removed with success',
            'remove favorite'
        )
    }
}
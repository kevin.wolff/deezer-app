import Axios from 'axios'
import state from './State.js'

export default class Album {

    // Get albums.
    static async get(accountId) {

        const result = await Axios.get(`http://127.0.0.1:8080/get/album/${accountId}`)
        if (result.data.error || result.status !== 200) {
            return new state(
                'error',
                result.data.error ? result.data.error.message : 'error when getting albums',
                'get albums'
            )
        }

        return new state(
            'success',
            'got albums with success',
            'get albums',
            result.data.data
        )
    }

    // Add album.
    static async add(accountId, albumId, token) {

        const result = await Axios.get(`http://127.0.0.1:8080/add/album/${accountId}/${albumId}/${token}`)
        if (result.data.error || result.status !== 200) {
            return new state(
                'error',
                result.data.error.message ? result.data.error.message : 'error when adding album',
                'add album'
            )
        }

        return new state(
            'success',
            'album added with success',
            'add album'
        )
    }

    // Remove album.
    static async delete(accountId, albumId, token) {

        const result = await Axios.get(`http://127.0.0.1:8080/delete/album/${accountId}/${albumId}/${token}`)
        if (result.data.error || result.status !== 200) {
            return new state(
                'error',
                result.data.error.message ? result.data.error.message : 'error when removing album',
                'remove album'
            )
        }

        return new state(
            'success',
            'album removed with success',
            'remove album'
        )
    }
}
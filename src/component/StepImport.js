import {useContext, useEffect, useState} from 'react'
import AppContext from '../DeezerContext.js'
import crushContent from '../function/crushContent.js'
import importContent from '../function/importContent.js'
import StepProgress from './StepProgress.js'
import state from '../class/State.js'

export default function StepImport() {
    const [importSuccess, setImportSuccess] = useState(Boolean)
    const [inputValue, setInputValue] = useState(String)
    const [deleteCurrentPlaylist, setDeleteCurrentPlaylist] = useState(Boolean)
    const [crushPlaylistSuccess, setCrushPlaylistSuccess] = useState(Boolean)
    const [deleteCurrentFavorite, setDeleteCurrentFavorite] = useState(Boolean)
    const [crushFavoriteSuccess, setCrushFavoriteSuccess] = useState(Boolean)
    const [deleteCurrentArtists, setDeleteCurrentArtists] = useState(Boolean)
    const [crushArtistsSuccess, setCrushArtistsSuccess] = useState(Boolean)
    const [deleteCurrentAlbums, setDeleteCurrentAlbums] = useState(Boolean)
    const [crushAlbumsSuccess, setCrushAlbumsSuccess] = useState(Boolean)

    const {accessToken, currentAccountId, setCurrentAccountId, content} = useContext(AppContext)

    useEffect(() => {
        setCurrentAccountId(inputValue)
    }, [inputValue, setCurrentAccountId])

    function handleCurrentIdInputChange(event) {
        setInputValue(event.target.value)
    }

    function handleDeletePlaylistCheckboxChange() {
        deleteCurrentPlaylist ? setDeleteCurrentPlaylist(false) : setDeleteCurrentPlaylist(true)
    }

    function handleDeleteFavoriteCheckboxChange() {
        deleteCurrentFavorite ? setDeleteCurrentFavorite(false) : setDeleteCurrentFavorite(true)
    }

    function handleDeleteArtistsCheckboxChange() {
        deleteCurrentArtists ? setDeleteCurrentArtists(false) : setDeleteCurrentArtists(true)
    }

    function handleDeleteAlbumsCheckboxChange() {
        deleteCurrentAlbums ? setDeleteCurrentAlbums(false) : setDeleteCurrentAlbums(true)
    }

    async function handleImport(event) {
        event.preventDefault()
        setImportSuccess(new state('loading', 'import du contenu'))
        if (!accessToken || content.length === 0) {
            setImportSuccess(new state('error', 'information from step one and two are missing', 'importing content'))
        } else {
            if (deleteCurrentPlaylist) {
                setCrushPlaylistSuccess(new state('loading', 'écrasement des playlists'))
                const crushResult = await crushContent(currentAccountId, accessToken, 'playlist')
                setCrushPlaylistSuccess(crushResult)
            }
            if (deleteCurrentFavorite) {
                setCrushFavoriteSuccess(new state('loading', 'écrasement des coups de coeur'))
                const crushResult = await crushContent(currentAccountId, accessToken, 'favorite')
                setCrushFavoriteSuccess(crushResult)
            }
            if (deleteCurrentArtists) {
                setCrushArtistsSuccess(new state('loading', 'écrasement des artistes'))
                const crushResult = await crushContent(currentAccountId, accessToken, 'artist')
                setCrushArtistsSuccess(crushResult)
            }
            if (deleteCurrentAlbums) {
                setCrushAlbumsSuccess(new state('loading', 'écrasement des albums'))
                const crushResult = await crushContent(currentAccountId, accessToken, 'album')
                setCrushAlbumsSuccess(crushResult)
            }
            const importResult = await importContent(accessToken, currentAccountId, content)
            setImportSuccess(importResult)
        }
    }

    return (
        <div className="container">
            <h2>Etape 3</h2>
            <form onSubmit={handleImport}>
                <label htmlFor="currentdAccountId">
                    Identifiant du nouveau compte
                    <input type="text" name="currentdAccountId" onChange={handleCurrentIdInputChange}
                           required="required"
                           placeholder="..."/>
                </label>
                <label htmlFor="deletePlaylist" className="checkbox">
                    <input type="checkbox" id="deletePlaylist" name="deletePlaylist"
                           onChange={handleDeletePlaylistCheckboxChange}/>
                    écraser playlists
                </label>
                <label htmlFor="deleteFavorite" className="checkbox">
                    <input type="checkbox" id="deleteFavorite" name="deleteFavorite"
                           onChange={handleDeleteFavoriteCheckboxChange}/>
                    écraser coups de coeur
                </label>
                <label htmlFor="deleteArtists" className="checkbox">
                    <input type="checkbox" id="deleteArtists" name="deleteArtists"
                           onChange={handleDeleteArtistsCheckboxChange}/>
                    écraser artistes
                </label>
                <label htmlFor="deleteAlbums" className="checkbox">
                    <input type="checkbox" id="deleteAlbums" name="deleteAlbums"
                           onChange={handleDeleteAlbumsCheckboxChange}/>
                    écraser albums
                </label>
                <input type="submit" value="Importer le contenu"/>
            </form>
            <div className="info">
                <StepProgress step={crushPlaylistSuccess}/>
                <StepProgress step={crushFavoriteSuccess}/>
                <StepProgress step={crushArtistsSuccess}/>
                <StepProgress step={crushAlbumsSuccess}/>
                <StepProgress step={importSuccess}/>
            </div>
        </div>
    )
}
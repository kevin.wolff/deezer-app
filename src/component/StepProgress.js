import {MdCheck, MdErrorOutline, MdHourglassEmpty, MdLibraryMusic, MdMic, MdAlbum} from 'react-icons/md'
import {FaHeart} from 'react-icons/fa'

export default function StepProgress(props) {

    if (props.step.status === 'success_auth') {
        return (
            <div className="info">
                <div className="wrapper text">
                    <p>Authentifié avec succès</p>
                    <span className="rightIcon"><MdCheck/></span>
                </div>
            </div>
        )
    }

    if (props.step.status === 'loading') {
        return (
            <div className="info">
                <div className="wrapper text">
                    <span className="leftIcon"><MdHourglassEmpty/></span>
                    <p>{props.step.msg}</p>
                </div>
            </div>
        )
    }

    if (props.step.status === 'error') {
        return (
            <div className="info">
                <div className="wrapper text">
                    <span className="leftIcon"><MdErrorOutline/></span>
                    <p>{props.step.status}</p>
                </div>
                <p>{props.step.msg}</p>
            </div>
        )
    }

    if (props.step.status === 'success_export') {
        return (
            <div className="info">
                <div className="wrapper text">
                    <p>{props.step.data.favoriteTracksLength}{props.step.data.favoriteTracksLength > 1 ? ' coups de coeur' : ' coup de coeur'}</p>
                    <span className="rightIcon"><FaHeart/></span>
                </div>
                <div className="wrapper text">
                    <p>{props.step.data.playlistsLength}{props.step.data.playlistsLength > 1 ? ' playlists' : ' playlist'}</p>
                    <span className="rightIcon"><MdLibraryMusic/></span>
                </div>
                <div className="wrapper text">
                    <p>{props.step.data.artistsLength}{props.step.data.artistsLength > 1 ? ' artistes' : ' artiste'}</p>
                    <span className="rightIcon"><MdMic/></span>
                </div>
                <div className="wrapper text">
                    <p>{props.step.data.albumsLength}{props.step.data.albumsLength > 1 ? ' albums' : ' album'}</p>
                    <span className="rightIcon"><MdAlbum/></span>
                </div>
            </div>
        )
    }

    if (props.step.status === 'success_import') {
        return (
            <div className="wrapper text">
                <p>Import validé</p>
                <span className="rightIcon"><MdCheck/></span>
            </div>
        )
    }

    if (props.step.status === 'success_crush_playlist') {
        return (
            <div className="wrapper text">
                <p>Ecrasement playlists</p>
                <span className="rightIcon"><MdCheck/></span>
            </div>
        )
    }

    if (props.step.status === 'success_crush_favorite') {
        return (
            <div className="wrapper text">
                <p>Ecrasement coups de coeur</p>
                <span className="rightIcon"><MdCheck/></span>
            </div>
        )
    }

    if (props.step.status === 'success_crush_artist') {
        return (
            <div className="wrapper text">
                <p>Ecrasement artistes</p>
                <span className="rightIcon"><MdCheck/></span>
            </div>
        )
    }

    if (props.step.status === 'success_crush_album') {
        return (
            <div className="wrapper text">
                <p>Ecrasement albums</p>
                <span className="rightIcon"><MdCheck/></span>
            </div>
        )
    }

    return (
        <div className="info"/>
    )
}
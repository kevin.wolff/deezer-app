import {useContext, useEffect, useState} from 'react'
import AppContext from '../DeezerContext.js'
import exportContent from '../function/exportContent.js'
import StepProgress from './StepProgress.js'
import State from '../class/State.js'

export default function StepExport() {
    const [exportSuccess, setExportSuccess] = useState(Boolean)
    const [inputValue, setInputValue] = useState(String)
    const {oldAccountId, setOldAccountId, setContent} = useContext(AppContext)

    useEffect(() => {
        setOldAccountId(inputValue)
    }, [inputValue, setOldAccountId])

    function handleInputChange(event) {
        setInputValue(event.target.value)
    }

    async function handleExport(event) {
        event.preventDefault()
        setExportSuccess(new State('loading', 'chargement du contenu'))
        const exportResult = await exportContent(oldAccountId)
        if (exportResult.status === 'success_export') {
            setContent({
                playlists: exportResult.data.playlists,
                artists: exportResult.data.artists,
                albums: exportResult.data.albums
            })
        }
        setExportSuccess(exportResult)
    }

    return (
        <div className="container">
            <h2>Etape 2</h2>
            <form onSubmit={handleExport}>
                <label htmlFor="oldAccountId">
                    Identifiant de l'ancien compte
                    <input type="text" name="oldAccountId" onChange={handleInputChange} required="required"
                           placeholder="..."/>
                </label>
                <input type="submit" value="Exporter le contenu"/>
            </form>
            <StepProgress step={exportSuccess}/>
        </div>
    )
}
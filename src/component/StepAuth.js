import {useContext, useEffect, useState} from 'react'
import {useLocation} from 'react-router-dom'
import {SiDeezer} from 'react-icons/si'
import AppContext from '../DeezerContext.js'
import StepProgress from './StepProgress.js'
import state from '../class/State.js'

export default function StepAuth() {
    const [authSuccess, setAuthSuccess] = useState(Boolean)
    const {accessToken, setAccessToken} = useContext(AppContext)

    const query = useQuery()

    function useQuery() {
        return new URLSearchParams(useLocation().search)
    }

    useEffect(() => {
        setAccessToken(query.get('access_token'))
    }, [query, setAccessToken])

    useEffect(() => {
        if (accessToken) {
            setAuthSuccess(new state('success_auth'))
        }
    }, [accessToken, setAuthSuccess])

    return (
        <div className="container">
            <h2>Etape 1</h2>
            <div className="wrapper">
                <a href="http://127.0.0.1:8080/connexion">
                    <div className="wrapper link btn">
                        <p>Connexion à Deezer</p>
                        <span className="rightIcon">
                            <SiDeezer/>
                        </span>
                    </div>
                </a>
            </div>
            <StepProgress step={authSuccess}/>
        </div>
    )
}
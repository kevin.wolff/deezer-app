import React from 'react'
import ReactDOM from 'react-dom'
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'
import App from './App.js'

ReactDOM.render(
    <React.StrictMode>
        <Router>
            <Switch>
                <Route exact path="/app" component={App}/>
            </Switch>
        </Router>
    </React.StrictMode>,
    document.getElementById('root')
)
